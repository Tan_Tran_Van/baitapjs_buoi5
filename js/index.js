//DOM ID
function domID(id) {
  return document.getElementById(id);
}

// Bài 1: Quản Lý Tuyển Sinh
domID("btn-result").onclick = function () {
  //input
  var diemChuan = domID("txt-diem-chuan").value * 1;
  var khuVuc = domID("txt-khu-vuc").value * 1;
  var doiTuong = domID("txt-doi-tuong").value * 1;

  //diem cac mon thi
  var diemMon1 = domID("txt-diem-mon-1").value * 1;
  var diemMon2 = domID("txt-diem-mon-2").value * 1;
  var diemMon3 = domID("txt-diem-mon-3").value * 1;

  //ouput
  var result = "";

  //progress
  if (diemMon1 <= 0 || diemMon2 <= 0 || diemMon3 <= 0) {
    result = "Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0";
  } else {
    var tongDiem = diemMon1 + diemMon2 + diemMon3 + khuVuc + doiTuong;
    if (tongDiem >= diemChuan) {
      result = `Bạn đã đậu. Tổng điểm: ${tongDiem}`;
    } else {
      result = `Bạn đã rớt. Tổng điểm: ${tongDiem}`;
    }
  }
  domID("xuat-ket-qua").innerHTML = `Kết quả: ${result}`;
};

//Bài 2: Tính Tiền Điện

domID("btn-tinh-tien-dien").onclick = function () {
  //input
  var hoTen = domID("txt-ho-ten").value;
  var soKw = domID("txt-so-Kw-dien").value * 1;

  // output
  var tienDien = 0;
  //progress
  if (soKw <= 50) {
    tienDien = soKw * 500;
  } else if (soKw > 50 && soKw <= 100) {
    tienDien = 50 * 500 + (soKw - 50) * 650;
  } else if (soKw > 100 && soKw <= 200) {
    tienDien = 50 * 500 + 50 * 650 + (soKw - 100) * 850;
  } else if (soKw > 200 && soKw <= 350) {
    tienDien = 50 * 500 + 50 * 650 + 100 * 850 + (soKw - 200) * 1100;
  } else {
    tienDien =
      50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (soKw - 350) * 1300;
  }
  domID(
    "xuat-ket-qua-dien"
  ).innerHTML = `Kết quả: Họ Tên: ${hoTen}; Tổng tiền điện: ${tienDien.toLocaleString()} vnđ`;
};

//Bài 3: Tính Thuế Thu Nhập Cá Nhân
domID("btn-tinh-thue").onclick = function () {
  //input
  var hoTen = domID("txt-ten").value;
  var tongThuNhap = domID("txt-tong-thu-nhap").value * 1;
  var soNguoiPhuThuoc = domID("txt-nguoi-phu-thuoc").value * 1;

  // output
  var thueTNCN = 0;

  //progress
  var tongThuNhapChiuThue = tongThuNhap - 4e6 - soNguoiPhuThuoc * 1.6e6;
  thueTNCN = tinhThue(tongThuNhapChiuThue);

  domID(
    "xuat-ket-qua-tinh-thue"
  ).innerHTML = `Kết quả: Họ tên: ${hoTen}; Tiền thuế thu nhập cá nhân: ${thueTNCN.toLocaleString()}`;
};

function tinhThue(thuNhap) {
  if (thuNhap > 0 && thuNhap <= 60e6) {
    //5%
    thue = thuNhap * 0.05;
    return thue;
  } else if (thuNhap > 60e6 && thuNhap <= 120e6) {
    //10%
    thue = 60e6 * 0.05 + (thuNhap - 60e6) * 0.1;
    return thue;
  } else if (thuNhap > 120e6 && thuNhap <= 210e6) {
    //15%
    thue = 60e6 * 0.05 + 60e6 * 0.1 + (thuNhap - 120e6) * 0.15;
    return thue;
  } else if (thuNhap > 210e6 && thuNhap <= 384e6) {
    //15% 180e6 * 0.15
    thue = 60e6 * 0.05 + 60e6 * 0.1 + 90e6 * 0.15 + (thuNhap - 210e6) * 0.2;
    return thue;
  } else if (thuNhap > 384e6 && thuNhap <= 624e6) {
    //15%
    thue =
      60e6 * 0.05 +
      60e6 * 0.1 +
      90e6 * 0.15 +
      174e6 * 0.2 +
      (thuNhap - 384e6) * 0.25;
    return thue;
  } else if (thuNhap > 624e6 && thuNhap <= 960e6) {
    //15%
    thue =
      60e6 * 0.05 +
      60e6 * 0.1 +
      90e6 * 0.15 +
      174e6 * 0.2 +
      240e6 * 0.25 +
      (thuNhap - 624e6) * 0.3;
    return thue;
  } else if (thuNhap > 960e6) {
    //15%
    thue =
      60e6 * 0.05 +
      60e6 * 0.1 +
      90e6 * 0.15 +
      174e6 * 0.2 +
      240e6 * 0.25 +
      336e6 * 0.3 +
      (thuNhap - 960e6) * 0.35;
    return thue;
  }
}

//Bài 4: Tính Tiền Cáp
domID("txt-loai-khach-hang").onchange = function () {
  var loaiKhachHang = domID("txt-loai-khach-hang").value;
  if (loaiKhachHang == 2) {
    domID("form-ket-noi").classList.remove("d-none");
    domID("form-ket-noi").classList.add("d-block");
  } else {
    domID("form-ket-noi").classList.remove("d-block");
    domID("form-ket-noi").classList.add("d-none");
  }
};

domID("btn-tinh-tien-cap").onclick = function () {
  //input
  var loaiKhachHang = domID("txt-loai-khach-hang").value;
  var maKhachHang = domID("txt-ma-khach-hang").value;
  var soKenh = domID("txt-so-kenh").value * 1;
  var soKetNoi = domID("txt-so-ket-noi").value * 1;
  //output
  var tienCap = 0;
  if (loaiKhachHang == 0) {
    alert("Vui Lòng chọn loại khách hàng!");
  } else if (loaiKhachHang == 2) {
    //doanh nghiep
    if (soKetNoi <= 10) {
      tienCap = 15 + 75 + 50 * soKenh;
    } else {
      tienCap = 15 + (75 + (soKetNoi - 10) * 5) + 50 * soKenh;
    }
  } else {
    //nhà dân
    tienCap = 4.5 + 20.5 + 7.5 * soKenh;
  }
  domID(
    "xuat-ket-qua-tien-cap"
  ).innerHTML = `Kết quả: Mã khách hàng: ${maKhachHang}; Tổng Tiền cáp: $ ${tienCap.toLocaleString()} USD`;
};
